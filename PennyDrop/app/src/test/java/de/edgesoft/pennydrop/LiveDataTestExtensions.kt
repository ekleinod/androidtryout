package de.edgesoft.pennydrop

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

fun <T> LiveData<T>.getOrAwaitValue(
    timeOut: Long = 2,
    timeUnit: TimeUnit = TimeUnit.SECONDS
): T? {

    var value: T? = null

    val latch = CountDownLatch(1)

    val observer = object : Observer<T> {
        override fun onChanged(newValue: T?) {
            value = newValue
            latch.countDown()
            this@getOrAwaitValue.removeObserver(this)
        }
    }

    observeForever(observer)

    if (!latch.await(timeOut, timeUnit)) {
        this@getOrAwaitValue.removeObserver(observer)
        return null
    }

    return value

}
