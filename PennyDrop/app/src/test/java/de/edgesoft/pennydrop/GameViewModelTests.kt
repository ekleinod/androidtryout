package de.edgesoft.pennydrop

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import de.edgesoft.pennydrop.data.entities.Player
import de.edgesoft.pennydrop.types.AI
import de.edgesoft.pennydrop.viewmodels.GameViewModel2
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class GameViewModelTests {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var gameViewModel: GameViewModel2
    private lateinit var testPlayers: List<Player>

    @Before
    fun initializeViewModel() {
        gameViewModel = GameViewModel2()
        testPlayers = listOf(
            Player(1, "Michael", true),
            Player(2, "Emily", true),
            Player(3, "Hazel", true),
            Player(4, "Riverboat Ron", false, selectedAI = AI.basicAI[5])
        )
    }

    @Test
    fun `Test startGame on GameViewModel2`() {

        gameViewModel.slots.value?.let { slots ->
            assertEquals(6, slots.size)
            assertEquals(5, slots.count { it.canBeFilled })
            assertEquals(0, slots.count { it.isFilled })
        } ?: fail("The slots on GameViewModel2 are null.")

        assertNull(gameViewModel.currentPlayer.value)

        assertTrue(gameViewModel.canRoll.value == false)
        assertTrue(gameViewModel.canPass.value == false)

        assertEquals("", gameViewModel.currentTurnText.value)
        assertEquals("", gameViewModel.currentStandingsText.value)


        gameViewModel.startGame(testPlayers)


        gameViewModel.slots.value?.let { slots ->
            assertEquals(6, slots.size)
            assertEquals(5, slots.count { it.canBeFilled })
            assertEquals(0, slots.count { it.isFilled })
        } ?: fail("The slots on GameViewModel2 are null.")

        assertEquals("Michael", gameViewModel.currentPlayer.value?.playerName)

        assertTrue(gameViewModel.canRoll.value == true)
        assertTrue(gameViewModel.canPass.value == false)

        assertEquals("The game has begun!\n", gameViewModel.currentTurnText.value)
        assertNotEquals("", gameViewModel.currentStandingsText.value)
        assertEquals("Current Standings:\n" +
                "\tMichael - 10 pennies\n" +
                "\tEmily - 10 pennies\n" +
                "\tHazel - 10 pennies\n" +
                "\tRiverboat Ron - 10 pennies", gameViewModel.currentStandingsText.value)

    }

    @Test
    fun `Test roll on GameViewModel2`() {

        gameViewModel.startGame(testPlayers)

        gameViewModel.roll()

        gameViewModel.slots.getOrAwaitValue()?.let { slots ->

            assertNotNull(slots)

            val lastRolledSlot = slots.firstOrNull{ it.lastRolled }
            assertNotNull(lastRolledSlot)

            val expectedFilledSlots = if (lastRolledSlot?.number == 6) 0 else 1
            assertEquals(expectedFilledSlots, slots.count{ it.isFilled })
            if (expectedFilledSlots > 0) {
                assertEquals(slots.firstOrNull{ it.isFilled }, lastRolledSlot)
            }

        } ?: fail("The slots on GameViewModel2 are null.")

        gameViewModel.currentPlayer.getOrAwaitValue()?.let { player ->
            assertEquals("Michael", player.playerName)
            assertEquals(10, player.pennies) // wrong assertion, but Model2!
        } ?: fail("The current player on GameViewModel2 is null.")

        gameViewModel.canRoll.getOrAwaitValue()?.let { canRoll ->
            assertEquals(true, canRoll)
        } ?: fail("canRoll on GameViewModel2 is null.")

        gameViewModel.canPass.getOrAwaitValue()?.let { canPass ->
            assertEquals(true, canPass)
        } ?: fail("canPass on GameViewModel2 is null.")

        gameViewModel.currentTurnText.getOrAwaitValue()?.let { currentTurnText ->
            val lastRolledSlot = gameViewModel.slots.value?.firstOrNull{ it.lastRolled }
            assertEquals(true, currentTurnText.contains("Michael rolled a ${lastRolledSlot?.number}"))
        } ?: fail("currentTurnText on GameViewModel2 is null.")

        gameViewModel.currentStandingsText.getOrAwaitValue()?.let { currentStandingsText ->
            assertEquals(true, currentStandingsText.contains("Michael - 10 pennies")) // wrong assertion, but Model2!
            assertEquals(true, currentStandingsText.contains("Emily - 10 pennies"))
            assertEquals(true, currentStandingsText.contains("Hazel - 10 pennies"))
            assertEquals(true, currentStandingsText.contains("Riverboat Ron - 10 pennies"))
        } ?: fail("currentStandingsText on GameViewModel2 is null.")

    }

}