package de.edgesoft.pennydrop

import de.edgesoft.pennydrop.data.entities.Player
import de.edgesoft.pennydrop.game.GameHandler
import de.edgesoft.pennydrop.game.TurnEnd
import de.edgesoft.pennydrop.types.AI
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.BeforeClass
import org.junit.Test

class GameHandlerTests {

    companion object {

        private lateinit var testPlayers: List<Player>

        @BeforeClass
        @JvmStatic
        fun setupTestPlayers() {
            testPlayers = listOf(
                Player(1, "Michael", true),
                Player(2, "Emily", true),
                Player(3, "Hazel", true),
                Player(4, "Riverboat Ron", false, selectedAI = AI.basicAI[5])
            )
        }

    }

    private fun checkNextPlayer(
        currentPlayer: Player,
        nextPlayer: Player
    ) = GameHandler.pass(testPlayers, currentPlayer).also { turnResult ->
            assertTrue(turnResult.playerChanged)
            assertEquals(TurnEnd.PASS, turnResult.turnEnd)
            assertEquals(currentPlayer, turnResult.previousPlayer)
            assertEquals(nextPlayer, turnResult.currentPlayer)
        }

    @Test
    fun `Test nextPlayer() via pass() function` () {

        val currentPlayer = testPlayers.first{it.playerName == "Emily"}
        val nextPlayer = testPlayers.first{it.playerName == "Hazel"}

        checkNextPlayer(currentPlayer, nextPlayer)

    }

    @Test
    fun `Test last nextPlayer() via pass() function` () {

        val currentPlayer = testPlayers.first{it.playerName == "Riverboat Ron"}
        val nextPlayer = testPlayers.first{it.playerName == "Michael"}

        checkNextPlayer(currentPlayer, nextPlayer)

    }

}