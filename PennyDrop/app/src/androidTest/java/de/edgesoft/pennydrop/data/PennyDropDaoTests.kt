package de.edgesoft.pennydrop.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.edgesoft.pennydrop.data.entities.Player
import de.edgesoft.pennydrop.getOrAwaitValueAndroid
import de.edgesoft.pennydrop.types.AI
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.junit.runner.RunWith
import java.util.concurrent.Executors

@RunWith(AndroidJUnit4::class)
class PennyDropDaoTests {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: PennyDropDatabase
    private lateinit var dao: PennyDropDao

    @Before
    fun initDatabaseAndDao() {

        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            PennyDropDatabase::class.java
        ).allowMainThreadQueries()
                .setTransactionExecutor(Executors.newSingleThreadExecutor())
                .build()

        dao = database.pennyDropDao()

    }

    @After
    fun closeDatabase() = database.close()


    @Test
    fun testInsertingNewPlayer() = runBlocking {
        val player = Player(5, "Hazel")

        Assert.assertNull(dao.getPlayer(player.playerName))

        val insertedPlayerId = dao.insertPlayer(player)

        Assert.assertEquals(player.playerId, insertedPlayerId)
        dao.getPlayer(player.playerName)?.let { newPlayer ->

            Assert.assertEquals(player.playerId, newPlayer.playerId)
            Assert.assertEquals(player.playerName, newPlayer.playerName)
            Assert.assertTrue(player.isHuman)

        } ?: Assert.fail("getPlayer is null.")

    }

    @Test
    fun testStartGame() = runBlocking {

        val players = listOf(
            Player(23, "Michael"),
            Player(12, "Emily"),
            Player(5, "Hazel"),
            Player(100, "Even Steven", false, AI.basicAI[4])
        )
        val pennyCount = 15

        val gameId = dao.startGame(players, pennyCount)

        dao.getCurrentGameWithPlayers().getOrAwaitValueAndroid()?.let { gameWithPlayers ->

            with(gameWithPlayers) {

                org.junit.Assert.assertEquals(gameId, game.gameId)
                org.junit.Assert.assertNotNull(game.startTime)
                org.junit.Assert.assertNull(game.endTime)
                org.junit.Assert.assertNull(game.lastRoll)
                org.junit.Assert.assertTrue(game.canRoll)
                org.junit.Assert.assertFalse(game.canPass)

            }

            players.forEach { player ->
                Assert.assertTrue(gameWithPlayers.players.contains(player))
            }

        } ?: Assert.fail("getCurrentGameWithPlayers is null.")

        val playerNames = players.map { it.playerName }
        playerNames.forEach { playerName ->
            Assert.assertNotNull(dao.getPlayer(playerName))
        }

        val playerIds = players.map { it.playerId }
        dao.getCurrentGameStatuses().getOrAwaitValueAndroid()?.let { gameStatuses ->

            Assert.assertTrue(gameStatuses.all { gameId == it.gameId })
            Assert.assertTrue(gameStatuses.all { playerIds.contains(it.playerId) })
            Assert.assertTrue(gameStatuses.all { pennyCount == it.pennies })
            Assert.assertEquals(1, gameStatuses.count { it.isRolling })
            Assert.assertEquals(
                players.first().playerId,
                gameStatuses.first { it.isRolling }.playerId
            )

        } ?: Assert.fail("getCurrentGameStatuses is null.")

    }

}