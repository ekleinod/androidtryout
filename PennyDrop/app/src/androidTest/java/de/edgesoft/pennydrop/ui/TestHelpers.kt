package de.edgesoft.pennydrop.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withParent
import de.edgesoft.pennydrop.R
import org.hamcrest.core.AllOf.allOf

fun typeInPlayerName(
    parentId: Int,
    name: String
) {
    onView(
        allOf(
            withId(R.id.edit_player_name),
            withParent(withId(parentId))
        )
    ).perform(typeText(name))
}

fun clickPlayerCheckBox(
    parentId: Int
) {
    onView(
        allOf(
            withId(R.id.checkbox_player_active),
            withParent(withId(parentId))
        )
    ).perform(click())
}
