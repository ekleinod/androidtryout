package de.edgesoft.pennydrop.ui

import android.view.View
import androidx.navigation.findNavController
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.closeSoftKeyboard
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.PositionAssertions.isCompletelyRightOf
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.edgesoft.pennydrop.MainActivity
import de.edgesoft.pennydrop.R
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.Description
import org.hamcrest.Matchers.anyOf
import org.hamcrest.Matchers.not
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.AllOf.allOf
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
class GameFragmentTests {

    private val coinSlotMap = mapOf(
        "1" to R.id.coinSlot1,
        "2" to R.id.coinSlot2,
        "3" to R.id.coinSlot3,
        "4" to R.id.coinSlot4,
        "5" to R.id.coinSlot5,
        "6" to R.id.coinSlot6
    )

    // start main activity before every test (without "Before" annotation)
    @get:Rule
    var activityScenarioRule = activityScenarioRule<MainActivity>()

    @Before
    fun startNewGameBeforeEachTest() = runBlockingTest {
        startGame(activityScenarioRule.scenario)
    }

    @ExperimentalCoroutinesApi
    fun startGame(scenario: ActivityScenario<MainActivity>) = runBlockingTest {

        scenario.onActivity { activity ->
            activity.findNavController(R.id.containerFragment).navigate(R.id.pickPlayersFragment)
        }

        typeInPlayerName(R.id.player1, "Michael")
        typeInPlayerName(R.id.player2, "Emily")
        clickPlayerCheckBox(R.id.player3)
        typeInPlayerName(R.id.player3, "Hazel")
        closeSoftKeyboard()

        onView(
            withId(R.id.buttonStartGame)
        ).perform(
            click()
        )

    }

    @Test
    fun checkStartingSlots() {

        coinSlotMap.forEach { (slotNumber, slotId) ->

            onView(
                allOf(
                    withId(R.id.slotNumberCoinSlot),
                    withParent(withId(slotId))
                )
            ).check(
                matches(withText(slotNumber))
            )

            onView(
                allOf(
                    withId(R.id.coinImageCoinSlot),
                    withParent(withId(slotId))
                )
            ).check(
                matches(not(isDisplayed()))
            )

            if (slotId != R.id.coinSlot6) {
                onView(
                    withId(R.id.coinSlot6)
                ).check(
                    isCompletelyRightOf(withId(slotId))
                )
            }

        }

    }

    @Test
    fun checkSingleRollResult() = runBlockingTest {

        onView(
            withId(R.id.rollButton)
        ).perform(
            click()
        )

        onView(
            withId(R.id.textCurrentPlayerName)
        ).check(
            matches(withText("Michael"))
        )

        onView(
            withId(R.id.textCurrentPlayerCoinsLeft)
        ).check(
            matches(withText("9"))
        )

        // just finding the view would be test enough as it would raise an error otherwise
        // because Espresso raises an error if multiple slots match the criteria
        onView(
            allOf(
                withId(R.id.bottomViewCoinSlot),
                isLastRolled(),

                // either the euro is displayed or we are slot 6
                // (both could be true, checking this would require an extra test)
                anyOf(
                    hasSibling(
                        allOf(
                            withId(R.id.coinImageCoinSlot),
                            isDisplayed()
                        )
                    ),
                    hasSibling(
                        allOf(
                            withId(R.id.slotNumberCoinSlot),
                            withText("6")
                        )
                    )
                )

            )
        ).check { view, noViewFoundException ->
            assertNotNull(view)
            assertNull(noViewFoundException)
        }

    }

    private fun isLastRolled() = object : TypeSafeMatcher<View>() {

        override fun describeTo(
            description: Description?
        ) {
            description?.appendText("The View is activated.")
        }

        override fun matchesSafely(
            item: View?
        ): Boolean = item?.isActivated == true

    }

}