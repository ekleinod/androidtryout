package de.edgesoft.pennydrop.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import de.edgesoft.pennydrop.data.PennyDropDatabase
import de.edgesoft.pennydrop.data.PennyDropRepository
import de.edgesoft.pennydrop.types.PlayerSummary

class RankingsViewModel(
    application: Application
): AndroidViewModel(application) {

    private val repository: PennyDropRepository
    val playerSummaries: LiveData<List<PlayerSummary>>

    init {

        repository = PennyDropDatabase
            .getDatabase(application, viewModelScope)
            .pennyDropDao()
            .let { dao ->
                PennyDropRepository.getInstance(dao)
            }

        playerSummaries = Transformations.map(
            repository.getCompletedGameStatusesWithPlayer()
        ) { gameStatusesWithPlayer ->

            gameStatusesWithPlayer
                .groupBy { it.player }
                .map { (player, statuses) ->
                    PlayerSummary(
                        id = player.playerId,
                        name = player.playerName,
                        gamesPlayed = statuses.count(),
                        wins = statuses.count { it.gameStatus.pennies == 0 },
                        isHuman = player.isHuman
                    )
                }
                .sortedWith(compareBy({ -it.wins }, { -it.gamesPlayed }))

        }

        playerSummaries.toString()

    }

}