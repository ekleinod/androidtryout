package de.edgesoft.pennydrop.data

import android.text.TextUtils
import androidx.room.TypeConverter
import de.edgesoft.pennydrop.data.entities.GameState
import de.edgesoft.pennydrop.types.AI
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

class Converters {

    private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME


    @TypeConverter
    fun toOffsetDateTime(
        value: String?
    ) = value?.let {
        formatter.parse(it, OffsetDateTime::from)
    }

    @TypeConverter
    fun fromOffsetDateTime(
        date: OffsetDateTime?
    ) = date?.format(formatter)


    @TypeConverter
    fun fromIntToGameState(
        gameStateInt: Int?
    ) = GameState.values().let { gameStates ->
        if ((gameStateInt != null) && gameStates.any { it.ordinal == gameStateInt }) {
            gameStates[gameStateInt]
        } else {
            GameState.UNKNOWN
        }
    }

    @TypeConverter
    fun fromGameStateToInt(
        gameState: GameState?
    ) = (gameState ?: GameState.UNKNOWN).ordinal


    @TypeConverter
    fun toIntList(
        value: String?
    ) = value?.split(",")?.let { values ->
        values
            .filter { !TextUtils.isEmpty(it) }
            .map { it.toInt() }
    } ?: emptyList()

    @TypeConverter
    fun fromListOfIntToString(
        numbers: List<Int>?
    ) = numbers?.joinToString(",") ?: ""


    @TypeConverter
    fun toAI(
        aiId: Long?
    ) = AI.basicAI.firstOrNull{ it.aiId == aiId }

    @TypeConverter
    fun fromAI(
        ai: AI?
    ) = ai?.aiId

}