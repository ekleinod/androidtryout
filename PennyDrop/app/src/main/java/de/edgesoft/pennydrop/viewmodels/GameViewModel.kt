package de.edgesoft.pennydrop.viewmodels

import android.app.Application
import androidx.lifecycle.*
import androidx.preference.PreferenceManager
import de.edgesoft.pennydrop.data.PennyDropDatabase
import de.edgesoft.pennydrop.data.PennyDropRepository
import de.edgesoft.pennydrop.data.entities.GameState
import de.edgesoft.pennydrop.data.entities.GameStatus
import de.edgesoft.pennydrop.data.entities.Player
import de.edgesoft.pennydrop.game.GameHandler
import de.edgesoft.pennydrop.game.GameWithPlayers
import de.edgesoft.pennydrop.game.TurnEnd
import de.edgesoft.pennydrop.game.TurnResult
import de.edgesoft.pennydrop.types.Slot
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.time.OffsetDateTime

class GameViewModel(
    application: Application
): AndroidViewModel(application) {

    private var clearText: Boolean = false

    private val repository: PennyDropRepository

    private val prefs = PreferenceManager.getDefaultSharedPreferences(application)

    val currentGame = MediatorLiveData<GameWithPlayers>()
    val currentGameStatuses: LiveData<List<GameStatus>>
    val currentPlayer: LiveData<Player>

    val slots: LiveData<List<Slot>>

    val currentStandingsText: LiveData<String>

    val canRoll: LiveData<Boolean>
    val canPass: LiveData<Boolean>


    init {

        repository = PennyDropDatabase
            .getDatabase(application, viewModelScope)
            .pennyDropDao()
            .let { dao ->
                PennyDropRepository.getInstance(dao)
            }

        currentGameStatuses = repository.getCurrentGameStatuses()

        currentGame.addSource(
            repository.getCurrentGameWithPlayers()
        ) { currentGameWithPlayers ->
            updateCurrentGame(currentGameWithPlayers, currentGameStatuses.value)
        }

        currentGame.addSource(
            currentGameStatuses
        ) { currentGameStatuses ->
            updateCurrentGame(currentGame.value, currentGameStatuses)
        }

        currentPlayer = Transformations.map(
            currentGame
        ) { gameWithPlayers ->
            gameWithPlayers?.players?.firstOrNull { player -> player.isRolling }
        }

        currentStandingsText = Transformations.map(
            currentGame
        ) { gameWithPlayers ->
            gameWithPlayers?.players?.let { players -> generateCurrentStandingsText(players) }
        }

        slots = Transformations.map(
            currentGame
        ) { gameWithPlayers ->
            Slot.mapFromGame(gameWithPlayers?.game)
        }

        canRoll = Transformations.map(
            currentPlayer
        ) { player ->
            (player?.isHuman == true) && (currentGame.value?.game?.canRoll == true)
        }

        canPass = Transformations.map(
            currentPlayer
        ) { player ->
            (player?.isHuman == true) && (currentGame.value?.game?.canPass == true)
        }

    }

    private fun updateCurrentGame(
        gameWithPlayers: GameWithPlayers?,
        gameStatuses: List<GameStatus>?
    ) {
        currentGame.value = gameWithPlayers?.updateStatuses(gameStatuses)
    }

    suspend fun startGame(
        playersForNewGame: List<Player>
    ) {
        repository.startGame(playersForNewGame, prefs?.getInt("pennyCount", Player.DEFAULT_PENNY_COUNT))
    }

    fun roll() {

        val theGame = currentGame.value?.game
        val thePlayers = currentGame.value?.players
        val theCurrentPlayer = currentPlayer.value
        val theSlots = slots.value

        if (
            (theGame != null) &&
            (thePlayers != null) &&
            (theCurrentPlayer != null) &&
            (theSlots != null) &&
            theGame.canRoll
        ) {
            updateFromGameHandler(GameHandler.roll(thePlayers, theCurrentPlayer, theSlots))
        }

    }

    fun pass() {

        val theGame = currentGame.value?.game
        val thePlayers = currentGame.value?.players
        val theCurrentPlayer = currentPlayer.value

        if (
            (theGame != null) &&
            (thePlayers != null) &&
            (theCurrentPlayer != null) &&
            theGame.canPass
        ) {
            updateFromGameHandler(GameHandler.pass(thePlayers, theCurrentPlayer))
        }

    }

    private fun updateFromGameHandler(
        result: TurnResult
    ) {

        val theGame = currentGame.value?.let { currentGameWithPlayers ->

            currentGameWithPlayers.game.copy(
                gameState = if (result.isGameOver) GameState.FINISHED else GameState.STARTED,
                lastRoll = result.lastRoll,
                filledSlots = updateFilledSlots(result, currentGameWithPlayers.game.filledSlots),
                currentTurnText = generateTurnText(result),
                canPass = result.canPass,
                canRoll = result.canRoll,
                endTime = if (result.isGameOver) OffsetDateTime.now() else null
            )

        } ?: return

        val theStatuses = currentGameStatuses.value?.map { status ->

            when (status.playerId) {

                result.previousPlayer?.playerId -> {
                    status.copy(
                        isRolling = false,
                        pennies = status.pennies + (result.coinChangeCount ?: 0)
                    )
                }

                result.currentPlayer?.playerId -> {
                    status.copy(
                        isRolling = !result.isGameOver,
                        pennies = status.pennies +
                                if (!result.playerChanged) { result.coinChangeCount ?: 0 } else 0
                    )
                }

                else -> status

            }

        } ?: emptyList()

        viewModelScope.launch {
            repository.updateGameAndStatuses(theGame, theStatuses)
            if (result.currentPlayer?.isHuman == false) {
                playAITurn()
            }
        }

    }

    private fun updateFilledSlots(
        result: TurnResult,
        filledSlots: List<Int>
    ) = when {
        result.clearSlots -> emptyList()
        ((result.lastRoll != null) && (result.lastRoll != 6)) -> filledSlots + result.lastRoll
        else -> filledSlots
    }

    private fun generateTurnText(
        result: TurnResult
    ): String {

        val currentText= if (clearText) "" else currentGame.value?.game?.currentTurnText ?: ""
        clearText = (result.turnEnd != null)

        val currentPlayerName = result.currentPlayer?.playerName ?: "???"

        return when {

            (result.isGameOver) -> generateGameOverText()

            (result.turnEnd == TurnEnd.BUST) -> "${ohNoPhrases.shuffled().first()} ${result.previousPlayer?.playerName} rolled a ${result.lastRoll} and is busted! They collected ${result.coinChangeCount} pennies for a total of ${result.previousPlayer?.pennies}\n" +
                    "$currentText"

            (result.turnEnd == TurnEnd.PASS) -> "${result.previousPlayer?.playerName} passed. They currently have ${result.previousPlayer?.pennies} pennies.\n" +
                    "$currentText"

            (result.lastRoll != null) -> "$currentPlayerName rolled a ${result.lastRoll}\n" +
                    "$currentText"

            else -> "missing TurnResult $result"
        }

    }

    private fun generateGameOverText(): String {

        val theStatuses = currentGameStatuses.value
        val thePlayers = currentGame.value?.players?.map { player ->
            player.apply {
                pennies = theStatuses?.firstOrNull { it.playerId == playerId }?.pennies ?: Player.DEFAULT_PENNY_COUNT
            }
        }

        val winningPlayer = thePlayers?.firstOrNull { !it.penniesLeft() || it.isRolling }?.apply { pennies = 0 }

        if ((thePlayers == null) || (winningPlayer == null)) return "N/A"

        return """
            |Game Over!
            |${winningPlayer.playerName} is the winner!
            |
            |${generateCurrentStandingsText(thePlayers, "Final Scores:\n")}
        """.trimMargin()

    }

    private fun generateCurrentStandingsText(
        players: List<Player>,
        headerText: String = "Current Standings:"
    ) = players.sortedBy { it.pennies }
        .joinToString(
            separator = "\n",
            prefix = "$headerText\n"
        ) {
            "\t${it.playerName} - ${it.pennies} pennies"
        }

    private suspend fun playAITurn() {

        delay(if (prefs.getBoolean("fastAI", false)) 100 else 1000)

        val theGame = currentGame.value?.game
        val thePlayers = currentGame.value?.players
        val theCurrentPlayer = currentPlayer.value
        val theSlots = slots.value

        if (
            (theGame != null) &&
            (thePlayers != null) &&
            (theCurrentPlayer != null) &&
            (theSlots != null)
        ) {
            GameHandler.playAITurn(
                thePlayers,
                theCurrentPlayer,
                theSlots,
                theGame.canPass
            )?.let { result ->
                updateFromGameHandler(result)
            }
        }

    }

    private val ohNoPhrases = listOf(
        "Oh no!",
        "Bummer!",
        "Dang.",
        "Whoops.",
        "Ah, fiddlesticks.",
        "Oh, kitty cats.",
        "Piffle.",
        "Well, crud.",
        "Ah, cinnamon bits.",
        "Ooh, bad luck.",
        "Shucks!",
        "Woopsie daisy.",
        "Nooooooo!",
        "Aw, rats and bats.",
        "Blood and thunder!",
        "Gee whillikins.",
        "Well that's disappointing.",
        "I find your lack of luck disturbing.",
        "That stunk, huh?",
        "Uff da."
    )

}