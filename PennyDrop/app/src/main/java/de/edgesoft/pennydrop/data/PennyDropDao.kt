package de.edgesoft.pennydrop.data

import androidx.lifecycle.LiveData
import androidx.room.*
import de.edgesoft.pennydrop.data.entities.Game
import de.edgesoft.pennydrop.data.entities.GameState
import de.edgesoft.pennydrop.data.entities.GameStatus
import de.edgesoft.pennydrop.data.entities.Player
import de.edgesoft.pennydrop.game.GameStatusWithPlayer
import de.edgesoft.pennydrop.game.GameWithPlayers
import java.time.OffsetDateTime

@Dao
abstract class PennyDropDao {

    @Query("""
        SELECT * FROM players 
        WHERE playerName = :playerName
    """)
    abstract fun getPlayer(
        playerName: String
    ): Player?

    @Transaction
    @Query("""
        SELECT * FROM games 
        ORDER BY startTime DESC 
        LIMIT 1
    """)
    abstract fun getCurrentGameWithPlayers(): LiveData<GameWithPlayers>

    @Transaction
    @Query("""
        SELECT * FROM game_statuses
        WHERE gameId = (
            SELECT gameId FROM games
            WHERE endTime IS NULL
            ORDER BY startTime DESC
            LIMIT 1
        )
        ORDER BY gamePlayerNumber 
    """)
    abstract fun getCurrentGameStatuses(): LiveData<List<GameStatus>>

    @Transaction
    @Query("""
        SELECT * FROM game_statuses gs
        WHERE gs.gameId IN (
            SELECT gameId FROM games
            WHERE gameState = :finishedGameState
        )
    """)
    abstract fun getCompletedGameStatusesWithPlayer(
        finishedGameState: GameState = GameState.FINISHED
    ): LiveData<List<GameStatusWithPlayer>>


    @Insert
    abstract suspend fun insertGame(
        game: Game
    ): Long

    @Insert
    abstract suspend fun insertPlayer(
        player: Player
    ): Long

    @Insert
    abstract suspend fun insertPlayers(
        players: List<Player>
    ): List<Long>

    @Insert
    abstract suspend fun insertGameStatuses(
        gameStatuses: List<GameStatus>
    )


    @Update
    abstract suspend fun updateGame(
        game: Game
    )

    @Update
    abstract suspend fun updateGameStatuses(
        gameStatuses: List<GameStatus>
    )

    @Transaction
    open suspend fun updateGameAndStatuses(
        game: Game,
        gameStatuses: List<GameStatus>
    ) {
        updateGame(game)
        updateGameStatuses(gameStatuses)
    }

    @Transaction
    open suspend fun startGame(
        players: List<Player>,
        pennyCount: Int? = null
    ): Long {

        closeOpenGames()

        val gameId = insertGame(
            Game(
                gameState = GameState.STARTED,
                currentTurnText = "The game has begun!\n",
                canRoll = true
            )
        )

        val playerIds = players.map { player ->
            getPlayer(player.playerName)?.playerId ?: insertPlayer(player)
        }

        insertGameStatuses(
            playerIds.mapIndexed { index, playerId ->

                GameStatus(
                    gameId,
                    playerId,
                    index,
                    index == 0,
                    pennyCount ?: Player.DEFAULT_PENNY_COUNT
                )

            }
        )

        return gameId

    }

    @Query("""
        UPDATE games 
        SET endTime = :endDate, gameState = :gameState
        WHERE endTime IS NULL
    """)
    abstract suspend fun closeOpenGames(
        endDate: OffsetDateTime = OffsetDateTime.now(),
        gameState: GameState = GameState.CANCELLED
    )

}