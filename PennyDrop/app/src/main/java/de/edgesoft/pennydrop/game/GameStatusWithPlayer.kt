package de.edgesoft.pennydrop.game

import androidx.room.Embedded
import androidx.room.Relation
import de.edgesoft.pennydrop.data.entities.GameStatus
import de.edgesoft.pennydrop.data.entities.Player

data class GameStatusWithPlayer(

    @Embedded
    val gameStatus: GameStatus,

    @Relation(
        parentColumn = "playerId",
        entityColumn = "playerId"
    )
    val player: Player
)