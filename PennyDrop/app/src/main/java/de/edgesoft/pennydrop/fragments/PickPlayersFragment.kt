package de.edgesoft.pennydrop.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import de.edgesoft.pennydrop.R
import de.edgesoft.pennydrop.databinding.FragmentPickPlayersBinding
import de.edgesoft.pennydrop.viewmodels.GameViewModel
import de.edgesoft.pennydrop.viewmodels.PickPlayersViewModel
import kotlinx.coroutines.launch

class PickPlayersFragment : Fragment() {

    private val pickPlayersViewModel by activityViewModels<PickPlayersViewModel>()
    private val gameViewModel by activityViewModels<GameViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentPickPlayersBinding
            .inflate(inflater, container, false)
            .apply {

                viewModel = pickPlayersViewModel

                buttonStartGame.setOnClickListener {
                    viewLifecycleOwner.lifecycleScope.launch {
                        gameViewModel.startGame(
                            pickPlayersViewModel.players.value
                                ?.filter { newPlayer -> newPlayer.isIncluded.get() }
                                ?.map { newPlayer -> newPlayer.toPlayer() }
                                ?: emptyList()
                        )
                        findNavController().navigate(R.id.gameFragment)
                    }
                }

            }

        return binding.root

    }

}