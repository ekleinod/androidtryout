package de.edgesoft.pennydrop.game

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import de.edgesoft.pennydrop.data.entities.Game
import de.edgesoft.pennydrop.data.entities.GameStatus
import de.edgesoft.pennydrop.data.entities.Player

data class GameWithPlayers(

    @Embedded
    val game: Game,

    @Relation(
        parentColumn = "gameId",
        entityColumn = "playerId",
        associateBy = Junction(GameStatus::class)
    )
    val players: List<Player>
) {

    fun updateStatuses(
        gameStatuses: List<GameStatus>?
    ) = if (gameStatuses != null) {
            this.copy(
                players = players.map { player ->
                    gameStatuses.firstOrNull { gameStatus ->
                        gameStatus.playerId == player.playerId
                    }?.let { gameStatus ->
                        player.apply {
                            pennies = gameStatus.pennies
                            isRolling = gameStatus.isRolling
                            gamePlayerNumber = gameStatus.gamePlayerNumber
                        }
                    } ?: player
                }.sortedBy { player ->
                    player.gamePlayerNumber
                }
            )
        } else {
            this
        }

}
