package de.edgesoft.pennydrop.types

import androidx.databinding.ObservableBoolean
import de.edgesoft.pennydrop.data.entities.Player

data class NewPlayer(
    var playerName: String = "",
    val isHuman: ObservableBoolean = ObservableBoolean(true),
    val canBeRemoved: Boolean = true,
    val canBeToggled: Boolean = true,
    var isIncluded: ObservableBoolean = ObservableBoolean(!canBeRemoved),
    var selectedAIPosition: Int = -1
) {

    fun selectedAI() = if (isHuman.get()) {
        null
    } else {
        AI.basicAI.getOrNull(selectedAIPosition)
    }

    fun toPlayer() = Player(
        playerName = if (isHuman.get()) playerName else selectedAI()?.name ?: "AI",
        isHuman = isHuman.get(),
        selectedAI = selectedAI()
    )

    override fun toString() = listOf(
        "name" to playerName,
        "isIncluded" to isIncluded.get(),
        "isHuman" to isHuman.get(),
        "canBeRemoved" to canBeRemoved,
        "canBeToggled" to canBeToggled,
        "selectedAI" to (selectedAI()?.name ?: "N/A")
    ).joinToString(", ", "NewPlayer(", ")") { (property, value) ->
        "$property=$value"
    }

}
