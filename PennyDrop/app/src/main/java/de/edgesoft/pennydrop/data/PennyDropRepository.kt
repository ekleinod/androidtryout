package de.edgesoft.pennydrop.data

import de.edgesoft.pennydrop.data.entities.Game
import de.edgesoft.pennydrop.data.entities.GameStatus
import de.edgesoft.pennydrop.data.entities.Player

class PennyDropRepository(
    private val pennyDropDao: PennyDropDao
) {

    fun getCurrentGameWithPlayers() = pennyDropDao.getCurrentGameWithPlayers()

    fun getCurrentGameStatuses() = pennyDropDao.getCurrentGameStatuses()

    fun getCompletedGameStatusesWithPlayer() = pennyDropDao.getCompletedGameStatusesWithPlayer()

    suspend fun startGame(
        players: List<Player>,
        pennyCount: Int? = null
    ) = pennyDropDao.startGame(players, pennyCount)

    suspend fun updateGameAndStatuses(
        game: Game,
        gameStatuses: List<GameStatus>
    ) = pennyDropDao.updateGameAndStatuses(game, gameStatuses)

    companion object {

        @Volatile
        private var instance: PennyDropRepository? = null

        fun getInstance(
            pennyDropDao: PennyDropDao
        ) = instance ?: synchronized(this) {
            instance ?: PennyDropRepository(pennyDropDao).also {
                instance = it
            }
        }

    }

}