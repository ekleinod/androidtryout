package de.edgesoft.pennydrop.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.edgesoft.pennydrop.types.NewPlayer

class PickPlayersViewModel : ViewModel() {

    val players = MutableLiveData<List<NewPlayer>>()
        .apply {
            value = (1..6).map { playerId ->
                NewPlayer(
                    canBeRemoved = playerId > 2,
                    canBeToggled = playerId > 1
                )
            }
        }

}