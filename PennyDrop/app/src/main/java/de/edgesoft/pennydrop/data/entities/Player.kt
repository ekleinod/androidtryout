package de.edgesoft.pennydrop.data.entities

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import de.edgesoft.pennydrop.types.AI

@Entity(tableName = "players")
data class Player(

    @PrimaryKey(autoGenerate = true)
    var playerId: Long = 0,

    val playerName: String = "",
    val isHuman: Boolean = true,
    val selectedAI: AI? = null

) {

    @Ignore
    var pennies: Int = DEFAULT_PENNY_COUNT
    @Ignore
    var isRolling: Boolean = false
    @Ignore
    var gamePlayerNumber: Int = -1

    fun penniesLeft(
        subtractPenny: Boolean = false
    ) = (pennies - (if (subtractPenny) 1 else 0)) > 0

    companion object {

        const val DEFAULT_PENNY_COUNT = 10

    }

}
