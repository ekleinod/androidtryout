package de.edgesoft.pennydrop.data.entities

enum class GameState {
    STARTED,
    FINISHED,
    CANCELLED,
    UNKNOWN
}