package de.edgesoft.pennydrop

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.preference.PreferenceManager
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity: AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(
        savedInstanceState: Bundle?
    ) {

        super.onCreate(savedInstanceState)

        // theme has to be set before setContentView
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val themeId = when (prefs.getString("theme", "Main")) {
            "Kotlin" -> R.style.Theme_Kotlin
            "PinkSurprise" -> R.style.Theme_PinkSurprise
            else -> R.style.Theme_Main
        }
        setTheme(themeId)

        // night mode
        val nightMode = when (prefs.getString("themeMode", "System")) {
            "Light" -> AppCompatDelegate.MODE_NIGHT_NO
            "Dark" -> AppCompatDelegate.MODE_NIGHT_YES
            else -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
        }

        AppCompatDelegate.setDefaultNightMode(nightMode)

        // view
        setContentView(R.layout.activity_main)

        // navigation
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.containerFragment) as NavHostFragment
        navController = navHostFragment.navController
        val bottomNav = findViewById<BottomNavigationView>(R.id.bottom_nav)
        bottomNav.setupWithNavController(navController)

        // exclude bottom nav from navigation back arrows (top left)
        val appBarConfiguration = AppBarConfiguration(bottomNav.menu)
        setupActionBarWithNavController(navController, appBarConfiguration)

    }

    override fun onCreateOptionsMenu(
        menu: Menu?
    ): Boolean {

        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.options, menu)

        return true

    }

    override fun onOptionsItemSelected(
        item: MenuItem
    ): Boolean =
        if (this::navController.isInitialized) {
            item.onNavDestinationSelected(navController) || super.onOptionsItemSelected(item)
        } else {
            false
        }

    override fun onSupportNavigateUp(): Boolean =
        (this::navController.isInitialized && navController.navigateUp())
                ||
        super.onSupportNavigateUp()

}