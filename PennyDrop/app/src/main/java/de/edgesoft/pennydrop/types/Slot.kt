package de.edgesoft.pennydrop.types

import de.edgesoft.pennydrop.data.entities.Game

data class Slot(
    val number: Int,
    val canBeFilled: Boolean = true,
    var isFilled: Boolean = false,
    var lastRolled: Boolean = false
) {

    companion object {

        fun mapFromGame(
            game: Game?
        ) = (1..6).map { slotNumber ->

            Slot(
                number = slotNumber,
                canBeFilled = (slotNumber != 6),
                isFilled = game?.filledSlots?.contains(slotNumber) ?: false,
                lastRolled = (game?.lastRoll == slotNumber)
            )

        }

    }

}

fun List<Slot>.clear() = this.forEach { slot ->
    slot.isFilled = false
    slot.lastRolled = false
}

fun List<Slot>.fullSlots(): Int = this.count { slot ->
    slot.canBeFilled && slot.isFilled
}