# Android Tryouts

Tests with android programming.

- [LARC](LARC) - Room DB for LARCs
- [meineersteapp](meineersteapp) - just playing
- [PennyDrop](PennyDrop) - Example from [Kotlin and Android Development featuring Jetpack](https://devtalk.com/books/kotlin-and-android-development-featuring-jetpack/)


## Legal stuff

### Licenses

License of the documents: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
See file [LICENSE](LICENSE).

License of the programs: GNU General Public License.
See file [COPYING](COPYING).

Which means:

- the documents are free, as long as you
	- don't make money with them
	- mention the creator
	- share derivates with the same license
- programs are free and open source
	- you can use the program as you want, even commercially
	- you can share the program as you like
	- if you change the source code, you have to distribute it under the same license, and you have to provide the source code of the programs


### Copyright

Copyright 2021-2021 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License.

See [COPYING](COPYING) for details.

This file is part of Android Tryouts.

Android Tryouts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Android Tryouts is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Android Tryouts. If not, see <http://www.gnu.org/licenses/>.
