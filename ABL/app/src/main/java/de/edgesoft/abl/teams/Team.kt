package de.edgesoft.abl.teams

data class Team (
    val id: String,
    val city: String,
    val nickname: String,
    val division: Division,
    val wins: Int = 0,
    val losses: Int = 0,
    val leagueRank: Int = -1,
    val divisionRank: Int = -1
) {

    companion object {
        val Appleton = Team("APL", "Appleton", "Foxes", Division.EAST)
        val Baraboo = Team("BOO", "Baraboo", "Big Tops", Division.WEST)
        val EauClaire = Team("EC", "Eau Claire", "Blues", Division.WEST)
        val GreenBay = Team("GB", "Green Bay", "Skyline", Division.EAST)
        val LaCrosse = Team("LAX", "La Crosse", "Lovers", Division.WEST)
        val LakeDelton = Team("LD", "Lake Delton", "Breakers", Division.WEST)
        val Madison = Team("MSN", "Madison", "Snowflakes", Division.WEST)
        val Milwaukee = Team("MKE", "Milwaukee", "Sunrise", Division.EAST)
        val Pewaukee = Team("PKE", "Pewaukee", "Princesses", Division.EAST)
        val Shawano = Team("SHW", "Shawano", "Shorebirds", Division.EAST)
        val SpringGreen = Team("SG", "Spring Green", "Thespians", Division.WEST)
        val SturgeonBay = Team("SB", "Sturgeon Bay", "Elders", Division.EAST)
        val Waukesha = Team("WAU", "Waukesha", "Riffs", Division.EAST)
        val WisconsinRapids = Team("WR", "Wisconsin Rapids", "Cranberries", Division.WEST)
    }

}