package de.edgesoft.abl.teams

enum class Division {
    EAST,
    WEST,
    UNKNOWN
}