package de.edgesoft.larc

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PimpleTypeDao {

    @Insert
    suspend fun insert(pimpleType: PimpleType): Long

    @Query("SELECT * FROM PimpleType ORDER BY name ASC")
    fun getAllPimpleTypes(): LiveData<List<PimpleType>>

    @Query("DELETE FROM PimpleType")
    fun deleteAll()

}
