package de.edgesoft.larc

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class PimpleTypeRepository(
    private val pimpleTypeDao: PimpleTypeDao
) {

    val allPimpleTypes: LiveData<List<PimpleType>> = pimpleTypeDao.getAllPimpleTypes()

    @WorkerThread
    suspend fun insert(pimpleType: PimpleType) {
        pimpleTypeDao.insert(pimpleType)
    }

}