package de.edgesoft.larc

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import kotlinx.coroutines.MainScope

class NewCoveringActivity : AppCompatActivity() {

    private lateinit var txtCoveringNumber: EditText
    private lateinit var txtCoveringName: EditText
    private lateinit var spnCoveringPimpleType: Spinner
    private lateinit var txtCoveringColor: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_covering)

        txtCoveringNumber = findViewById(R.id.edit_number)
        txtCoveringName = findViewById(R.id.edit_name)
        spnCoveringPimpleType = findViewById(R.id.spn_pimple_type)
        txtCoveringColor = findViewById(R.id.edit_color)

        val dbLARC = LARCRoomDatabase.getDatabase(this, MainScope())

        dbLARC
            .pimpleTypeDao()
            .getAllPimpleTypes()
            .observe(this,
                Observer { lstPimpleTypes ->
                    ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item,
                        lstPimpleTypes
                    ).also { adapter ->
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        spnCoveringPimpleType.adapter = adapter
                    }
                }
            )

        val btnSave = findViewById<Button>(R.id.button_save)
        btnSave.setOnClickListener {
            val replyIntent = Intent()

            if (TextUtils.isEmpty(txtCoveringNumber.text) ||
                TextUtils.isEmpty(txtCoveringName.text) ||
                (spnCoveringPimpleType.selectedItem == null)) {

                setResult(Activity.RESULT_CANCELED, replyIntent)

            } else {

                replyIntent.putExtra(EXTRA_REPLY_NUMBER, txtCoveringNumber.text.toString().toInt())
                replyIntent.putExtra(EXTRA_REPLY_NAME, txtCoveringName.text.toString())
                replyIntent.putExtra(EXTRA_REPLY_PIMPLE_TYPE_ID, (spnCoveringPimpleType.selectedItem as? PimpleType)?.id)
                replyIntent.putExtra(EXTRA_REPLY_COLOR, txtCoveringColor.text.toString())

                setResult(Activity.RESULT_OK, replyIntent)

            }

            finish()

        }

    }

    companion object {
        const val EXTRA_REPLY_NUMBER = "de.edgesoft.larc.REPLY.covering.number"
        const val EXTRA_REPLY_NAME = "de.edgesoft.larc.REPLY.covering.name"
        const val EXTRA_REPLY_PIMPLE_TYPE_ID = "de.edgesoft.larc.REPLY.covering.pimple_type_id"
        const val EXTRA_REPLY_COLOR = "de.edgesoft.larc.REPLY.covering.color"
    }

}
