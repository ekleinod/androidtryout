package de.edgesoft.larc

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PimpleTypeListAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<PimpleTypeListAdapter.PimpleTypeViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var pimpleTypes = emptyList<PimpleType>()

    inner class PimpleTypeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val pimpleTypeItemView: TextView = itemView.findViewById(R.id.pimple_type_view_text)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PimpleTypeViewHolder {
        val itemView = inflater.inflate(R.layout.pimple_type_view, parent, false)
        return PimpleTypeViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PimpleTypeViewHolder, position: Int) {
        val current = pimpleTypes[position]
        holder.pimpleTypeItemView.text = "[${current.id}] ${current.name}"
    }

    internal fun setPimpleTypes(pimpleTypes: List<PimpleType>) {
        this.pimpleTypes = pimpleTypes
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = pimpleTypes.size

}