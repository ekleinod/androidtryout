package de.edgesoft.larc

import android.content.Context
import androidx.room.*

@Entity(
    indices = [Index(value = ["name"]), Index(value = ["pimple_type_id"])],
    foreignKeys = [ForeignKey(entity = PimpleType::class, parentColumns = ["id"], childColumns = ["pimple_type_id"])]
)
data class Covering (
    var number: Int,
    var name: String,
    @ColumnInfo(name = "pimple_type_id") var pimpleTypeId: Int,
    var color: String?
) {
    @PrimaryKey(autoGenerate = true) var id: Int = 0

    fun toDisplay(context: Context): String {
        return context.getString(R.string.covering_display, id, number, name, pimpleTypeId, color)
    }
}
