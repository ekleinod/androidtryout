package de.edgesoft.larc

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface CoveringDao {

    @Insert
    suspend fun insert(covering: Covering)

    @Query("SELECT * FROM Covering ORDER BY number ASC")
    fun getAllCoverings(): LiveData<List<Covering>>

    @Query("SELECT * FROM Covering WHERE name LIKE :name LIMIT 1")
    fun getCoveringByName(name: String): LiveData<Covering>

    @Query("DELETE FROM Covering")
    fun deleteAll()

}
