package de.edgesoft.larc

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CoveringViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: CoveringRepository

    val allCoverings: LiveData<List<Covering>>

    init {
        val coveringsDao = LARCRoomDatabase.getDatabase(application, viewModelScope).coveringDao()
        repository = CoveringRepository(coveringsDao)

        allCoverings = repository.allCoverings
    }

    fun insert(covering: Covering) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(covering)
    }

}