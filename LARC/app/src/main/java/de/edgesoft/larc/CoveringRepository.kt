package de.edgesoft.larc

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class CoveringRepository(
    private val coveringDao: CoveringDao
) {

    val allCoverings: LiveData<List<Covering>> = coveringDao.getAllCoverings()

    @WorkerThread
    suspend fun insert(covering: Covering) {
        coveringDao.insert(covering)
    }

}