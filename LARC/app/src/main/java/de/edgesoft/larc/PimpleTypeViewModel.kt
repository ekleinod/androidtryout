package de.edgesoft.larc

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PimpleTypeViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: PimpleTypeRepository

    val allPimpleTypes: LiveData<List<PimpleType>>

    init {
        val pimpleTypeDao = LARCRoomDatabase.getDatabase(application, viewModelScope).pimpleTypeDao()
        repository = PimpleTypeRepository(pimpleTypeDao)

        allPimpleTypes = repository.allPimpleTypes
    }

    fun insert(pimpleType: PimpleType) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(pimpleType)
    }

}