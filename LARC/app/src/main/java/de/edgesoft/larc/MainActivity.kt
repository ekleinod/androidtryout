package de.edgesoft.larc

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var coveringViewModel: CoveringViewModel
    private lateinit var pimpleTypeViewModel: PimpleTypeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener {
            val intent = Intent(this@MainActivity, NewCoveringActivity::class.java)
            startActivityForResult(intent, newCoveringActivityRequestCode)
        }

        val coveringsRecyclerView = findViewById<RecyclerView>(R.id.coverings_view)
        val coveringsAdapter = CoveringListAdapter(this)
        coveringsRecyclerView.adapter = coveringsAdapter
        coveringsRecyclerView.layoutManager = LinearLayoutManager(this)

        coveringViewModel = ViewModelProviders.of(this).get(CoveringViewModel::class.java)
        coveringViewModel.allCoverings.observe(this, Observer {coverings ->
            coverings?.let { coveringsAdapter.setCoverings(it) }
        })

        val pimpleTypeRecyclerView = findViewById<RecyclerView>(R.id.pimple_types_view)
        val pimpleTypeAdapter = PimpleTypeListAdapter(this)
        pimpleTypeRecyclerView.adapter = pimpleTypeAdapter
        pimpleTypeRecyclerView.layoutManager = LinearLayoutManager(this)

        pimpleTypeViewModel = ViewModelProviders.of(this).get(PimpleTypeViewModel::class.java)
        pimpleTypeViewModel.allPimpleTypes.observe(this, Observer {pimpleTypes ->
            pimpleTypes?.let { pimpleTypeAdapter.setPimpleTypes(it) }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val newCoveringActivityRequestCode = 1
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if ((requestCode == newCoveringActivityRequestCode) && (resultCode == Activity.RESULT_OK)) {
            data?.let {
                val covering = Covering(
                    it.getIntExtra(NewCoveringActivity.EXTRA_REPLY_NUMBER, -1),
                    it.getStringExtra(NewCoveringActivity.EXTRA_REPLY_NAME)!!,
                    it.getIntExtra(NewCoveringActivity.EXTRA_REPLY_PIMPLE_TYPE_ID, -1),
                    it.getStringExtra(NewCoveringActivity.EXTRA_REPLY_COLOR)
                )
                coveringViewModel.insert(covering)
            }
        } else {
            Toast.makeText(
                applicationContext,
                R.string.empty_not_saved,
                Toast.LENGTH_LONG
            ).show()
        }

    }

}
