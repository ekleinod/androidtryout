package de.edgesoft.larc

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CoveringListAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<CoveringListAdapter.CoveringViewHolder>() {

    private val theContext = context
    private val inflater = LayoutInflater.from(theContext)
    private var coverings = emptyList<Covering>()

    inner class CoveringViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val coveringItemView: TextView = itemView.findViewById(R.id.covering_view_text)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoveringViewHolder {
        val itemView = inflater.inflate(R.layout.covering_view, parent, false)
        return CoveringViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CoveringViewHolder, position: Int) {
        val current = coverings[position]
        holder.coveringItemView.text = current.toDisplay(theContext)
    }

    internal fun setCoverings(coverings: List<Covering>) {
        this.coverings = coverings
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = coverings.size

}