package de.edgesoft.larc

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities = [Covering::class, PimpleType::class], version = 1)
abstract class LARCRoomDatabase : RoomDatabase() {

    abstract fun coveringDao(): CoveringDao
    abstract fun pimpleTypeDao(): PimpleTypeDao

    // define singleton
    companion object {

        @Volatile
        private var INSTANCE: LARCRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): LARCRoomDatabase {

            return INSTANCE ?: synchronized(this) {

                // create database
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    LARCRoomDatabase::class.java,
                    "LARC_database"
                ).addCallback(LARCDatabaseCallback(scope))
                    .build()

                INSTANCE = instance
                instance

            }

        }

        private class LARCDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let {database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(
                            database.coveringDao(),
                            database.pimpleTypeDao()
                        )
                    }
                }
            }

        }

        suspend fun populateDatabase(
            coveringDao: CoveringDao,
            pimpleTypeDao: PimpleTypeDao
        ) {

            // first delete data without hurting foreign keys
            coveringDao.deleteAll()
            pimpleTypeDao.deleteAll()

            // then add data without hurting foreign keys either
            val pt1 = pimpleTypeDao.insert(PimpleType("in")).toInt()
            val pt2 = pimpleTypeDao.insert(PimpleType("out")).toInt()

            coveringDao.insert(Covering(100, "Testbelag 1", pt1, null))
            coveringDao.insert(Covering(101, "Testbelag 2", pt2, "rot"))

        }

    }

}