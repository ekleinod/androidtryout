package de.edgesoft.larc

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    indices = [Index(value = ["name"], unique = true)]
)
data class PimpleType (
    var name: String
) {
    @PrimaryKey(autoGenerate = true) var id: Int = 0

    override fun toString(): String {
        return name
    }

}
